---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

My name is Bhavin Shah.

### my history

Here's a quick rundown of some work experiences:

- Computer Vision with Tensorflow, Pytorch + FastAI
- C#/C++ 3D Applications in Unity and Unreal Engines
- PHP/SQL/Java Web Services hosted on OpenStack, orchestrated by Scalr
- Data visualization with d3.js
- Embedded software with Arduinos and Jetson TX2
- CAM/CNC programming with OneCNC and operation of HAAS Mill
